'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _styleHelper = require('../../../../utils/style-helper');

var _styleHelper2 = _interopRequireDefault(_styleHelper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _defaultExport = ['.tag--existing{display:inline-block;padding:5px;max-width:100%;box-sizing:border-box;position:relative;}', '.tag--existing button{outline:none;display:inline-block;vertical-align:baseline;border:none;border-radius:3px;background:' + _styleHelper2.default.primaryColor + ';padding:5px;color:#ffffff;}', '.tag--existing button i{font-size:0.75rem;}', '.tag--existing .tag-input__suggestions:before{content:\'\';position:absolute;top:0 !important;left:20px !important;width:0;height:0;margin:0 !important;-webkit-transform-origin:0 0;-ms-transform-origin:0 0;transform-origin:0 0;-webkit-transform:rotate(135deg);-ms-transform:rotate(135deg);transform:rotate(135deg);box-sizing:border-box !important;box-shadow:-4px 4px 14px 0 rgba(0,0,0,0.05) !important;border-width:6px !important;border-style:solid !important;border-color:transparent transparent #ffffff #ffffff !important;}'];
_defaultExport.__hash = '1784301946';
_defaultExport.__scoped = ['.tag--existing.jsx-2502888891{display:inline-block;padding:5px;max-width:100%;box-sizing:border-box;position:relative;}', '.tag--existing.jsx-2502888891 button.jsx-2502888891{outline:none;display:inline-block;vertical-align:baseline;border:none;border-radius:3px;background:' + _styleHelper2.default.primaryColor + ';padding:5px;color:#ffffff;}', '.tag--existing.jsx-2502888891 button.jsx-2502888891 i.jsx-2502888891{font-size:0.75rem;}', '.tag--existing.jsx-2502888891 .tag-input__suggestions:before{content:\'\';position:absolute;top:0 !important;left:20px !important;width:0;height:0;margin:0 !important;-webkit-transform-origin:0 0;-ms-transform-origin:0 0;transform-origin:0 0;-webkit-transform:rotate(135deg);-ms-transform:rotate(135deg);transform:rotate(135deg);box-sizing:border-box !important;box-shadow:-4px 4px 14px 0 rgba(0,0,0,0.05) !important;border-width:6px !important;border-style:solid !important;border-color:transparent transparent #ffffff #ffffff !important;}'];
_defaultExport.__scopedHash = '2502888891';
exports.default = _defaultExport;