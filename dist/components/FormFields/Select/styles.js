'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _styleHelper = require('../../../utils/style-helper');

var _styleHelper2 = _interopRequireDefault(_styleHelper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _defaultExport = ['select::-ms-expand{display:none;}', '.select-container{position:relative;}', '.select-container select{padding-right:30px;}', '.select-container .material-icons{position:absolute;right:15px;top:calc(50% - 12px);height:12px;width:12px;color:' + _styleHelper2.default.borderColor + ';}'];
_defaultExport.__hash = '1835436016';
_defaultExport.__scoped = ['select.jsx-3634566321::-ms-expand{display:none;}', '.select-container.jsx-3634566321{position:relative;}', '.select-container.jsx-3634566321 select.jsx-3634566321{padding-right:30px;}', '.select-container.jsx-3634566321 .material-icons{position:absolute;right:15px;top:calc(50% - 12px);height:12px;width:12px;color:' + _styleHelper2.default.borderColor + ';}'];
_defaultExport.__scopedHash = '3634566321';
exports.default = _defaultExport;