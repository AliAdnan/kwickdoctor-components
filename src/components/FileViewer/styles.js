import css from 'styled-jsx/css';

export default css`
  .image {
    height: 100%;
  }
`;
